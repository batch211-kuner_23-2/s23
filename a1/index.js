console.log("Hello World");

let trainer = {
	Name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu","Charizzard","Squirtle","Bulbasaur"],
	Friend: {
	hoenn: ["May","Max"],
	kanto: ["Brock","Misty"]
	},

talk: function(){
	console.log("Pikachu! I choose you!");
	}
};
console.log(trainer);
console.log('Result of dot notation: ');
console.log(trainer.Name);

console.log('Result of sqaure bracket notation: ');
console.log(trainer.Pokemon);

console.log('Result of talk method: ');
trainer.talk();


function Pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level * 1.5;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);

target.health -= this.attack
	console.log(target.name + " healht is now reduced to " + target.health);

		if(target.health <= 0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + " has fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let newtwo = new Pokemon("Newtwo", 100);
console.log(newtwo);


