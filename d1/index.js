console.log("Hello World");

// Object
/*
---- an object is a data type that is used to represent real world objects

"key" - "property" of an object
"value" - actual data to be stored
*/

// Two ways in creating objects in JS
/*
1.Object literal notation
let object = {}

2.Object Constructor Notation
object instantation(let object = new object())
*/

// Object Literal Notation

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log('Result from creating objects using literal notation');
console.log(cellphone);

// Mini Activity 1
let cellphone1 = {
	name: "Alcatel",
	manufactureDate: 1995
};

console.log('Result from creating objects using literal notation');
console.log(cellphone1);


let ninja = {
	name: "Naruto Uzumaki",
	village: "Konoha",
	childred: ["Boruto","Himawari"]
};
console.log(ninja);

// Object Constructor Notation -- create objects using a constructor function.

// "this" keyword refers to the properties within the object
function Laptop(name,manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}
// create an instance object using the laptop constructor
let laptop = new Laptop("Lenovo",2008);
console.log('Result from creating objects using object constructor');
console.log(laptop);

// the "new" operator creates an instance of an object(new Object)
let myLaptop = new Laptop("Macbook Air",2020);
console.log('Result from creating objects using object constructor');
console.log(myLaptop);


// Mini Activity 2

let myLaptop1 = new Laptop("Dell",2000);
console.log('Result from creating objects using object constructor');
console.log(myLaptop1);

let myLaptop2 = new Laptop("Toshiba",2005);
console.log('Result from creating objects using object constructor');
console.log(myLaptop2);

let myLaptop3 = new Laptop("Acer",2010);
console.log('Result from creating objects using object constructor');
console.log(myLaptop3);


let oldLaptop = Laptop("Portal R2E CCMC",1980);
console.log('Result from creating objects using object constructor');
console.log(oldLaptop); // 'undefined' kasi walang "new" operator


// Create empty objects

let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);


// Accessing Object Properties


// using dot notation -- highly recommend
console.log("Result from dot notation: " + myLaptop.name);

// using square bracket notation
console.log("Result from square bracket notation: " + myLaptop["name"]);

//accessing array of objects
//accessing array of objects using square bracket notation and array indexes can cause confusion


let arrayObj = [laptop,myLaptop];
// using square bracket notation
console.log(arrayObj[0]["name"]);

// using dot notation
console.log(arrayObj[0].name);


// create an object using object literals
let car = {};
console.log("Current Value of car object: ");
console.log(car);


// initializing/adding object properties
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);


// initializing/adding object properties using bracket notation
console.log("Result from adding properties using bracket notation: ");
car["manufacture date"] = 2019;
console.log(car);

// delete object properties
delete car["manufacture date"];
console.log("Result from deleting properties: ");
console.log(car);

car["manufactureDate"] = 2019;
console.log(car);

// reassign object property values
car.name = "Toyota Vios";
console.log("Result from reassigning property values: ");
console.log(car);

// Object Methods -- a method is a function which is a property of an object

let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
}
console.log(person);
console.log("Result from object methods: ");
person.talk();


person.walk = function(steps){
	console.log(this.name + "walked " +  steps + " forward");
};
console.log(person);
person.walk(500);



let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas",
	},
	emails: ["joe@mail.com","joesmith@mail.xyz"],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + " " + this.lastName + " " + " I live in " + this.address.city + "," + this.address.country);
	}
}

friend.introduce();


function Pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);


// Mini activity 3
	target.health -= this.attack
	console.log(target.name + " healht is now reduced to " + target.health);

		if(target.health <= 0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 88);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 10);
console.log(rattata);